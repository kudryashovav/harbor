#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests, json, urllib3, datetime, re
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
green = '\033[1;32;48m'
normal = '\033[0;37;48m'
red =  '\033[1;31;48m'
#################################################################
#Обязательно заполнить переменные                               #
##                                                              #
#Удалять теги старше указанного количества дней                 #
dcnt = 30                                                       #
#URL API Harbor                                                 #
registry = 'https://harbor.telecom.ru'                          #
#swarm api                                                      #
swarm = 'https://swarm-manager.telecom.ru:8443'                 #
#Авторизационные данные Harbor                                  #
user = user                                                     #
passwd = 'Harbor12345'                                          #
#################################################################
st = datetime.datetime.today() - datetime.timedelta(days=dcnt)
print(st)
##
#При помощи API Docker получем список образов которые находятся в продуктововм использовании. Данные образы будут удалены из списка на удаление.
print(green + "Images in prod:" + normal)
prodimages = []
services = json.loads(requests.get(swarm + '/services', verify=False).text)
for service in services:
    if re.sub("https://","",registry) in re.sub("@.*$", "", service['Spec']['TaskTemplate']['ContainerSpec']['Image']):
	prodimages.append(re.sub(re.sub("https://","",registry) + "\/","",(re.sub(r"^" + registry + "\/", "",(re.sub("@.*$", "", service['Spec']['TaskTemplate']['ContainerSpec']['Image']))))))
print (prodimages)
##
#Работаем с Registry
#Получим список проектов
proj = json.loads(requests.get(registry +  '/api/projects', auth=(user,passwd), verify=False).text)

#Для каждого проекта, получим список репозиториев, создадим общий список.
repos = []
for p in proj:
    for repo in (json.loads(requests.get(registry + '/api/repositories?project_id=' + str(p['project_id']), auth=(user, passwd), verify=False).text)):
        repos.append(repo['name'])

# Получим список тегов старше указанной даты, у которых отсутствуют лейблы
tagslist = []
for r in repos:
    for t in (json.loads(requests.get(registry + '/api/repositories/' + r.replace('/', '%2F') + '/tags', auth=(user,passwd), verify=False).text)):
        if t["labels"] or t['created'] > st.strftime('%Y-%m-%d'):
            pass
        else: 
            tagslist.append(r + ":" + t['name'])

# Если образ отсутствует в продуктовой эксплуатации, удалим его.
for tag in tagslist:
    if tag in prodimages:
        pass
    else:
        print(tag + " removed...")
        requests.delete(registry + '/api/repositories/' + (tag.replace('/', '%2F')).replace(':','/tags/'), auth=(user, passwd), verify=False)