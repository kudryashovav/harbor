# LDAP
```
LDAP_URL: ldap://10.59.143.1 #Адрес LDAP
LDAP_SearchDN: ldap_read #пользователь, у которого есть разрешение на поиск в LDAP/AD
LDAP_Search_Pwd: •••••••••••••••••••• # Пароль пользователя
LDAP_BaseDN: DC=corp,DC=contoso,DC=loc #DN для поиска пользователей
LDAP_UID: sAMAccountName #Атрибут, используемый для сопоставления пользователя во время поиска LDAP
LDAP Scope: Subtree #Область для поиска пользователя
```