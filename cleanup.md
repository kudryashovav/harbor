# Удаление не используемых тегов в Harbor
Eсли регистр используется как для продуктовой эксплуатации, так и для test, staging, etc сред, в течении определенного промежутка времени назревает необходимость очистки приватного регистра от мусора.  
Были определены следующие условия очистки:
* Теги должны хранится не более определенного промежутка времени
* Теги используемые в продуктовой эксплуатации не должны быть удалены даже если они старше определенного промежука времени.
* Теги с лейблами не должны удалятся (base images, etc).

Приборка в регистре Harbor реализована через [скрипт](https://gitlab.com/KudryashovAV/harbor/blob/master/cleanup.py):  
* Через API Docker получаем список тегов образов используемых в продуктовой экплуатации.
```py
prodimages = []
services = json.load(requests.get(swarm + '/services', verify=False).text) #Получем список сервисов
for service in services: # Для каждого сервиса из сервисов запущенных в кластере
    if registry in re.sub("@.*$", "", service['Spec']['TaskTemplate']['ContainerSpec']['Image']): # Выберем только те образы, которые размещены в нашем реджистри
        prodimages.append(re.sub(r"^" + registry + "\/", "",(re.sub("@.*$", "", service['Spec']['TaskTemplate']['ContainerSpec']['Image'])))) # Приведем к виду - project/repository:tag
```
* Через API Harbor получаем список проектов,
```py
proj = json.loads(requests.get(api +  '/api/projects', auth=(user,passwd), verify=False).text)
```
репозиториев
```py
repos = []
for p in proj:
    for repo in (json.loads(requests.get(api + '/api/repositories?project_id=' + str(p['project_id']), auth=(user, passwd), verify=False).text)):
        repos.append(repo['name'])
```
и тегов старше указанной даты у которых отсутствуют лейблы
```py
tagslist = []
for r in repos:
    for t in (json.loads(requests.get(api + '/api/repositories/' + r.replace('/', '%2F') + '/tags', auth=(user,passwd), verify=False).text)):
        if t["labels"] and t['created'] < st.strftime('%Y-%m-%d'):
            print(t)
            pass
        else:
            tagslist.append(r + ":" + t['name'])
```
* Проверяем наличие тегов на использование в продуктовой эксплуатации, если не используются - удаляем.
```py
for tag in tagslist:
    if tag in prodimages:
        pass
    else:
        print (tag + " отсутствует в продуктовой эксплуатации - удален." )
        requests.delete(api + '/api/repositories/' + (tag.replace('/', '%2F')).replace(':','/tags/'), auth=(user, passwd), verify=False)
```

> Пространство не освобождается при удалении образов из регистра. Необходимо настроить [`Garbage Collection`](https://github.com/docker/docker.github.io/blob/master/registry/garbage-collection.md)  - его задача освободить пространство, удаляя объекты из файловой системы, на которые больше не ссылается манифест.