# Установка

Harbor может быть установлена одним из трех способов:
* Онлайн установка
* Оффлайн установка
* Установка в Kubernetes  
Все установщики можно скачать с официальной [release page](https://github.com/goharbor/harbor/releases).

# Скачаем и распакуем последнюю версию harbor
```sh
wget https://storage.googleapis.com/harbor-releases/release-1.7.0/harbor-online-installer-v1.7.4.tgz
tar xvf harbor-online-installer-v1.7.4.tgz
cd harbor
```
# Создадим собственный сертификат SSL
```sh
mkdir cert && cd cert
openssl req -sha256 -x509 -days 365 -nodes -newkey rsa:4096 -keyout  harbor.example.ru.key -out harbor.example.ru.crt
```
# Произведем первоначальную настройку
```sh
vi harbor.conf
hostname = harbor.example.ru
ui_url_protocol = https
ssl_cert = $pwd/cert/harbor.example.ru.crt
ssl_cert_key = $pwd/cert/harbor.example.ru.key
./install.sh
```
Вы можете получить доступ к серверу Harbour на http://harbor.example.ru

Если есть проблемы с авторизацией через `docker login` -> https://docs.docker.com/registry/insecure/