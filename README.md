# Harbor
1. [Установка](https://gitlab.com/KudryashovAV/harbor/blob/master/install.md)
2. [Настройка LDAP](https://gitlab.com/KudryashovAV/harbor/blob/master/ldap.md)
3. [Работа с API](https://gitlab.com/KudryashovAV/harbor/blob/master/api.md)
4. [Очистка registry](https://gitlab.com/KudryashovAV/harbor/blob/master/cleanup.md)