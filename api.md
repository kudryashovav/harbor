# Работа с API.
Cкопировать содержимое файла [swagger.yaml](https://github.com/goharbor/harbor/edit/master/docs/swagger.yaml) в http://editor.swagger.io/  
```bash
curl -u admin:password -i -k -X GET "https://harbor.mycompany.local/api/users?username=user1"
curl -i -k -X GET "https://harbor.mycompany.local/api/search?q=library/"
curl -i -k -X GET "https://harbor.mycompany.local/api/projects"
```
Дальше полученный JSON можно парсить с помощью [jq](https://stedolan.github.io/jq/tutorial/) и получить, например тэги по датам создания:
```sh
anatoly@hp:~$ curl -u admin:password -k -X GET "https://harbor.exemple.ru/api/repositories/devops%2Fdind/tags" | jq '.[0] | {tag: .name, created: .created}'
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   802  100   802    0     0   6314      0 --:--:-- --:--:-- --:--:--  6314
{
  "tag": "18.09.2",
  "created": "2019-02-25T09:25:38.730705919Z"
}
```